/* eslint-disable */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Home from '../components/home/home'
import $ from 'jquery'
window.jQuery = $
global.jQuery = $
class HomeContainer extends React.Component {
    constructor(props) {
        super(props)

    }

    componentDidMount() {
        $('.set-bg').each(function() {
            var bg = $(this).data('setbg');
            $(this).css('background-image', 'url(' + bg + ')');
        });

        // $('#carouselExample').on('slide.bs.carousel', function (e) {

  
        //     var $e = $(e.relatedTarget);
        //     var idx = $e.index();
        //     var itemsPerSlide = 4;
        //     var totalItems = $('.carousel-item').length;
            
        //     if (idx >= totalItems-(itemsPerSlide-1)) {
        //         var it = itemsPerSlide - (totalItems - idx);
        //         for (var i=0; i<it; i++) {
        //             // append slides to end
        //             if (e.direction=="left") {
        //                 $('.carousel-item').eq(i).appendTo('.carousel-inner');
        //             }
        //             else {
        //                 $('.carousel-item').eq(0).appendTo('.carousel-inner');
        //             }
        //         }
        //     }
        // });
        
        
          $(document).ready(function() {
        /* show lightbox when clicking a thumbnail */
            $('a.thumb').click(function(event){
              event.preventDefault();
              var content = $('.modal-body');
              content.empty();
                var title = $(this).attr("title");
                $('.modal-title').html(title);        
                content.html($(this).html());
                $(".modal-profile").modal({show:true});
            });
        
          });
    }
    
    render() {
        return (
            <div>
                <Home/>
            </div>
        )
    }
}
export default HomeContainer