import React, { Component } from "react";
import Login from "../components/login";

class LoginContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Login />
      </div>
    );
  }
}
export default LoginContainer;
