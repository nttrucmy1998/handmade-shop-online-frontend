import React, { Component } from "react";
import Blog from "../components/blog";

class BlogContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Blog />
      </div>
    );
  }
}
export default BlogContainer;
