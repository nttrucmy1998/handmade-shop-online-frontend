import React, { Component } from "react";
import Contact from "../components/contact";

class ContactContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Contact />
      </div>
    );
  }
}
export default ContactContainer;
