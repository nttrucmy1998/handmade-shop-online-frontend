import React, { Component } from "react";
import BlogDetail from "../components/blog-details";

class BlogDetailContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <BlogDetail />
      </div>
    );
  }
}
export default BlogDetailContainer;
