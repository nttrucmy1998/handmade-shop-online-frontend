import React, { Component } from "react";
import Register from "../components/register";

class RegisterContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Register />
      </div>
    );
  }
}
export default RegisterContainer;
