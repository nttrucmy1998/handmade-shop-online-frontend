import React, { Component } from "react";
import Product from "../components/product";
import $ from 'jquery'
window.jQuery = $
global.jQuery = $

class ProductContainer extends React.Component {
  constructor(props) {
    super(props);
  }
  

  componentDidMount() {
    $(document).ready(function () {
      // MDB Lightbox Init
      $(function () {
        $("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");
      });
    });
  }

  render() {
    return (
      <div>
        <Product />
      </div>
    );
  }
}
export default ProductContainer;
